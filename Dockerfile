FROM node:14

WORKDIR /app

COPY . .

ENV PORT=8080
EXPOSE 8080

CMD [ "node", "index.js" ]