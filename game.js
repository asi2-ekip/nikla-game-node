const { v4: uuidv4 } = require('uuid');
const { io } = require('.');

exports.Game = class Game {

    id = uuidv4();

    constructor(player1, player2){
        // Create socket room for this game
        player1.socket.join(this.id)
        player2.socket.join(this.id)

        io.to(this.id).emit('startGame')
    }

}