const http = require('http');
const { handleSocketConnection } = require('./socket');
const server = http.createServer()

const io = require('socket.io')(server, {
    cors: {
        origin: true,
        methods: ['GET', 'POST']
    }
});


io.on('connection', (socket) => handleSocketConnection(io, socket))

exports.io = io;




const port = process.env.PORT || 3002
server.listen(port).on('listening', () =>
    console.log('Listening on port', port)
)