const { Game } = require("./game");
const { Player } = require("./player");

const matchMaking = [];

exports.handleSocketConnection = function(io, socket){
    console.log("new client", socket.id);
    socket.emit('authRequest')

    socket.on('auth', (username) => {
        console.log(`[${username}] Auth`);
        socket.removeAllListeners('auth')
        // TODO replace auth with username + cards
        // TODO verify cards ownership with backend
        handleSocketMatchmaking(socket, username);
    })
}

handleSocketMatchmaking(socket, username){

    // TODO CARDS
    const newPlayer = new Player(socket, username, [])

    if(matchMaking.length === 0){
        matchMaking.push(newPlayer);
        return;
    }

    const waitingPlayer = matchMaking.shift();

    const game = new Game(waitingPlayer, newPlayer);

}
